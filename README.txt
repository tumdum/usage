g++ (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609
Copyright (C) 2015 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

MULTIMAP indexed by SET of STRING with RANDOM_KEYS compiled by g++
	User time (seconds): 16.40
	Maximum resident set size (kbytes): 4592
UNORDERED_MULTIMAP indexed by SET of STRING with RANDOM_KEYS compiled by g++
	User time (seconds): 16.31
	Maximum resident set size (kbytes): 4632
MAP indexed by SET of STRING with RANDOM_KEYS compiled by g++
	User time (seconds): 16.42
	Maximum resident set size (kbytes): 4600
UNORDERED_MAP indexed by SET of STRING with RANDOM_KEYS compiled by g++
	User time (seconds): 17.42
	Maximum resident set size (kbytes): 4540
MULTIMAP indexed by VECTOR of STRING with RANDOM_KEYS compiled by g++
	User time (seconds): 3.88
	Maximum resident set size (kbytes): 3656
UNORDERED_MULTIMAP indexed by VECTOR of STRING with RANDOM_KEYS compiled by g++
	User time (seconds): 3.78
	Maximum resident set size (kbytes): 3500
MAP indexed by VECTOR of STRING with RANDOM_KEYS compiled by g++
	User time (seconds): 3.98
	Maximum resident set size (kbytes): 3792
UNORDERED_MAP indexed by VECTOR of STRING with RANDOM_KEYS compiled by g++
	User time (seconds): 3.93
	Maximum resident set size (kbytes): 3660

MULTIMAP indexed by SET of BOOSTSTRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 15.68
	Maximum resident set size (kbytes): 4324
UNORDERED_MULTIMAP indexed by SET of BOOSTSTRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 16.11
	Maximum resident set size (kbytes): 4160
MAP indexed by SET of BOOSTSTRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 15.30
	Maximum resident set size (kbytes): 4216
UNORDERED_MAP indexed by SET of BOOSTSTRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 16.62
	Maximum resident set size (kbytes): 4320
MULTIMAP indexed by VECTOR of BOOSTSTRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 3.96
	Maximum resident set size (kbytes): 3320
UNORDERED_MULTIMAP indexed by VECTOR of BOOSTSTRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 4.66
	Maximum resident set size (kbytes): 3104
MAP indexed by VECTOR of BOOSTSTRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 3.95
	Maximum resident set size (kbytes): 3268
UNORDERED_MAP indexed by VECTOR of BOOSTSTRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 4.37
	Maximum resident set size (kbytes): 3200

MULTIMAP indexed by SET of STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 16.98
	Maximum resident set size (kbytes): 3952
UNORDERED_MULTIMAP indexed by SET of STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 16.09
	Maximum resident set size (kbytes): 4000
MAP indexed by SET of STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 15.48
	Maximum resident set size (kbytes): 3900
UNORDERED_MAP indexed by SET of STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 16.57
	Maximum resident set size (kbytes): 4024
MULTIMAP indexed by VECTOR of STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 4.05
	Maximum resident set size (kbytes): 3044
UNORDERED_MULTIMAP indexed by VECTOR of STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 3.89
	Maximum resident set size (kbytes): 3108
MAP indexed by VECTOR of STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 4.31
	Maximum resident set size (kbytes): 3236
UNORDERED_MAP indexed by VECTOR of STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 3.74
	Maximum resident set size (kbytes): 3172

MULTIMAP indexed by SET of STATIC_STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 12.77
	Maximum resident set size (kbytes): 3848
UNORDERED_MULTIMAP indexed by SET of STATIC_STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 13.26
	Maximum resident set size (kbytes): 3820
MAP indexed by SET of STATIC_STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 12.97
	Maximum resident set size (kbytes): 3836
UNORDERED_MAP indexed by SET of STATIC_STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 13.74
	Maximum resident set size (kbytes): 3928
MULTIMAP indexed by VECTOR of STATIC_STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 1.23
	Maximum resident set size (kbytes): 3076
UNORDERED_MULTIMAP indexed by VECTOR of STATIC_STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 1.00
	Maximum resident set size (kbytes): 2860
MAP indexed by VECTOR of STATIC_STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 1.14
	Maximum resident set size (kbytes): 3012
UNORDERED_MAP indexed by VECTOR of STATIC_STRINGREF with RANDOM_KEYS compiled by g++
	User time (seconds): 1.09
	Maximum resident set size (kbytes): 3112

MULTIMAP indexed by SET of STRINGPTR with RANDOM_KEYS compiled by g++
	User time (seconds): 12.82
	Maximum resident set size (kbytes): 3772
UNORDERED_MULTIMAP indexed by SET of STRINGPTR with RANDOM_KEYS compiled by g++
	User time (seconds): 12.92
	Maximum resident set size (kbytes): 3808
MAP indexed by SET of STRINGPTR with RANDOM_KEYS compiled by g++
	User time (seconds): 13.09
	Maximum resident set size (kbytes): 3920
UNORDERED_MAP indexed by SET of STRINGPTR with RANDOM_KEYS compiled by g++
	User time (seconds): 13.09
	Maximum resident set size (kbytes): 3908
MULTIMAP indexed by VECTOR of STRINGPTR with RANDOM_KEYS compiled by g++
	User time (seconds): 1.18
	Maximum resident set size (kbytes): 3092
UNORDERED_MULTIMAP indexed by VECTOR of STRINGPTR with RANDOM_KEYS compiled by g++
	User time (seconds): 1.02
	Maximum resident set size (kbytes): 3044
MAP indexed by VECTOR of STRINGPTR with RANDOM_KEYS compiled by g++
	User time (seconds): 1.28
	Maximum resident set size (kbytes): 3200
UNORDERED_MAP indexed by VECTOR of STRINGPTR with RANDOM_KEYS compiled by g++
	User time (seconds): 1.08
	Maximum resident set size (kbytes): 3216

MULTIMAP indexed by SET of INT with RANDOM_KEYS compiled by g++
	User time (seconds): 13.47
	Maximum resident set size (kbytes): 3920
UNORDERED_MULTIMAP indexed by SET of INT with RANDOM_KEYS compiled by g++
	User time (seconds): 13.21
	Maximum resident set size (kbytes): 3764
MAP indexed by SET of INT with RANDOM_KEYS compiled by g++
	User time (seconds): 12.96
	Maximum resident set size (kbytes): 3904
UNORDERED_MAP indexed by SET of INT with RANDOM_KEYS compiled by g++
	User time (seconds): 13.14
	Maximum resident set size (kbytes): 3828
MULTIMAP indexed by VECTOR of INT with RANDOM_KEYS compiled by g++
	User time (seconds): 1.14
	Maximum resident set size (kbytes): 3008
UNORDERED_MULTIMAP indexed by VECTOR of INT with RANDOM_KEYS compiled by g++
	User time (seconds): 1.09
	Maximum resident set size (kbytes): 3024
MAP indexed by VECTOR of INT with RANDOM_KEYS compiled by g++
	User time (seconds): 1.16
	Maximum resident set size (kbytes): 3116
UNORDERED_MAP indexed by VECTOR of INT with RANDOM_KEYS compiled by g++
	User time (seconds): 1.02
	Maximum resident set size (kbytes): 3144



MULTIMAP indexed by SET of STRING with KEYS_IN_ORDER compiled by g++
	User time (seconds): 2.30
	Maximum resident set size (kbytes): 4652
UNORDERED_MULTIMAP indexed by SET of STRING with KEYS_IN_ORDER compiled by g++
	User time (seconds): 2.16
	Maximum resident set size (kbytes): 4668
MAP indexed by SET of STRING with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.11
	Maximum resident set size (kbytes): 2888
UNORDERED_MAP indexed by SET of STRING with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.09
	Maximum resident set size (kbytes): 2864
MULTIMAP indexed by VECTOR of STRING with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.62
	Maximum resident set size (kbytes): 3656
UNORDERED_MULTIMAP indexed by VECTOR of STRING with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.62
	Maximum resident set size (kbytes): 3616
MAP indexed by VECTOR of STRING with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.04
	Maximum resident set size (kbytes): 2908
UNORDERED_MAP indexed by VECTOR of STRING with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.04
	Maximum resident set size (kbytes): 2816

MULTIMAP indexed by SET of BOOSTSTRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 2.26
	Maximum resident set size (kbytes): 4332
UNORDERED_MULTIMAP indexed by SET of BOOSTSTRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 2.10
	Maximum resident set size (kbytes): 4312
MAP indexed by SET of BOOSTSTRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.06
	Maximum resident set size (kbytes): 2824
UNORDERED_MAP indexed by SET of BOOSTSTRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.06
	Maximum resident set size (kbytes): 2848
MULTIMAP indexed by VECTOR of BOOSTSTRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.62
	Maximum resident set size (kbytes): 3312
UNORDERED_MULTIMAP indexed by VECTOR of BOOSTSTRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.57
	Maximum resident set size (kbytes): 3256
MAP indexed by VECTOR of BOOSTSTRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.10
	Maximum resident set size (kbytes): 2864
UNORDERED_MAP indexed by VECTOR of BOOSTSTRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.08
	Maximum resident set size (kbytes): 2792

MULTIMAP indexed by SET of STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 2.25
	Maximum resident set size (kbytes): 3896
UNORDERED_MULTIMAP indexed by SET of STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 2.23
	Maximum resident set size (kbytes): 4004
MAP indexed by SET of STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.09
	Maximum resident set size (kbytes): 2724
UNORDERED_MAP indexed by SET of STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.12
	Maximum resident set size (kbytes): 2852
MULTIMAP indexed by VECTOR of STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.63
	Maximum resident set size (kbytes): 2992
UNORDERED_MULTIMAP indexed by VECTOR of STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.48
	Maximum resident set size (kbytes): 3064
MAP indexed by VECTOR of STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.06
	Maximum resident set size (kbytes): 2868
UNORDERED_MAP indexed by VECTOR of STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.06
	Maximum resident set size (kbytes): 2796

MULTIMAP indexed by SET of STATIC_STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 1.73
	Maximum resident set size (kbytes): 3832
UNORDERED_MULTIMAP indexed by SET of STATIC_STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 1.58
	Maximum resident set size (kbytes): 3852
MAP indexed by SET of STATIC_STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.09
	Maximum resident set size (kbytes): 2728
UNORDERED_MAP indexed by SET of STATIC_STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.07
	Maximum resident set size (kbytes): 2700
MULTIMAP indexed by VECTOR of STATIC_STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.22
	Maximum resident set size (kbytes): 3080
UNORDERED_MULTIMAP indexed by VECTOR of STATIC_STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.15
	Maximum resident set size (kbytes): 2948
MAP indexed by VECTOR of STATIC_STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.07
	Maximum resident set size (kbytes): 2688
UNORDERED_MAP indexed by VECTOR of STATIC_STRINGREF with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.03
	Maximum resident set size (kbytes): 2716

MULTIMAP indexed by SET of STRINGPTR with KEYS_IN_ORDER compiled by g++
	User time (seconds): 1.80
	Maximum resident set size (kbytes): 3844
UNORDERED_MULTIMAP indexed by SET of STRINGPTR with KEYS_IN_ORDER compiled by g++
	User time (seconds): 1.65
	Maximum resident set size (kbytes): 3828
MAP indexed by SET of STRINGPTR with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.06
	Maximum resident set size (kbytes): 2788
UNORDERED_MAP indexed by SET of STRINGPTR with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.09
	Maximum resident set size (kbytes): 2704
MULTIMAP indexed by VECTOR of STRINGPTR with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.30
	Maximum resident set size (kbytes): 3084
UNORDERED_MULTIMAP indexed by VECTOR of STRINGPTR with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.13
	Maximum resident set size (kbytes): 2996
MAP indexed by VECTOR of STRINGPTR with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.09
	Maximum resident set size (kbytes): 2880
UNORDERED_MAP indexed by VECTOR of STRINGPTR with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.04
	Maximum resident set size (kbytes): 2824

MULTIMAP indexed by SET of INT with KEYS_IN_ORDER compiled by g++
	User time (seconds): 1.67
	Maximum resident set size (kbytes): 3784
UNORDERED_MULTIMAP indexed by SET of INT with KEYS_IN_ORDER compiled by g++
	User time (seconds): 1.49
	Maximum resident set size (kbytes): 3856
MAP indexed by SET of INT with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.09
	Maximum resident set size (kbytes): 2712
UNORDERED_MAP indexed by SET of INT with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.08
	Maximum resident set size (kbytes): 2724
MULTIMAP indexed by VECTOR of INT with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.19
	Maximum resident set size (kbytes): 2968
UNORDERED_MULTIMAP indexed by VECTOR of INT with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.10
	Maximum resident set size (kbytes): 2916
MAP indexed by VECTOR of INT with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.03
	Maximum resident set size (kbytes): 2816
UNORDERED_MAP indexed by VECTOR of INT with KEYS_IN_ORDER compiled by g++
	User time (seconds): 0.03
	Maximum resident set size (kbytes): 2820



