#!/bin/sh

./gen.py 800 > generated_types.h
mkdir -p bin

${CXX} --version

for key_gen in "RANDOM_KEYS" "KEYS_IN_ORDER"
do
    for key_type in "STRING" "BOOSTSTRINGREF" "STRINGREF" "STATIC_STRINGREF" "STRINGPTR" "INT"
    do
        for keys_base in "SET" "VECTOR"
        do
            for map in "MULTIMAP" "UNORDERED_MULTIMAP" "MAP" "UNORDERED_MAP"
            do
                # echo "${key_gen}/${key_type}/${keys_base}/${map}"
                echo "${map} indexed by ${keys_base} of ${key_type} with ${key_gen} compiled by ${CXX}"
                output=bin/usage_${key_gen}_${key_type}_${keys_base}_${map}_${CXX}
                if [ "${COMPILE}" ]
                then
                    ${CXX} -Wall -Wextra -std=c++11 -Os -DNDEBUG -D${key_gen} -D${key_type} -D${keys_base} -D${map} usage.cc -o ${output}
                    strip ${output}
                fi
                if [ "$RUN" ]
                then
                    /usr/bin/time -v ./${output} 2>&1 | grep -e "Maximum resident set" -e "User time"
                fi
            done
        done
        echo ""
    done
    echo "\n"
done
