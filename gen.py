#!/usr/bin/env python

import sys, random, string

N = 6

def type_name():
    return ''.join(random.choice(string.ascii_uppercase) for _ in range(N))

def main():
    count = int(sys.argv[1])
    print "// GENERATED FILE"
    print "// DO NOT MODIFY"
    m = {}
    for x in range(count):
        name = type_name()
        m[name] = x
        print
        print "// TYPE ", x
        print "const std::string %s = \"%s\";" % (name, name)
        print "const uint32_t %s_ID = %d;" % (name, x)
    print "\n\nconst int MAX_KEY_SIZE = %d;\n" % (count)
    print "const std::string& i2s(const uint32_t i) {"
    print "  switch(i) {"
    for k, v in m.iteritems():
        print "    case %s_ID: return %s;" % (k, k)
    print "  }"
    print "  assert(!\"unknown id\");"
    print "  std::abort();"
    print "}\n\n"
    

if __name__ == "__main__":
    main()
