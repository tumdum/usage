#include <algorithm>
#include <functional>
#include <map>
#include <random>
#include <set>
#include <string>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <cassert>
#include <cstdint>

#include <boost/utility/string_ref.hpp>
#include <boost/functional/hash.hpp>

#include "generated_types.h"

#if defined(STATIC_STRINGREF)
#define STRINGREF
#endif

namespace
{

const int ENTRIES = 2000;
const int MAX_INVOCATIONS = 4000;
const int MAX_KEYS_SIZE = 20;

std::mt19937 rng(42);
std::uniform_int_distribution<> keys_size_dist(1,MAX_KEYS_SIZE);
std::uniform_int_distribution<> key_size_dist(0,MAX_KEY_SIZE-1);
}

using StringRef = std::reference_wrapper<const std::string>;

namespace boost
{

template <>
struct hash<StringRef>
{
    size_t operator() (const StringRef& s) const
#if defined(STATIC_STRINGREF)
    { return std::hash<const std::string*>{}(&s.get()); }
#else
    { return std::hash<std::string>{}(s.get()); }
#endif
};

template <>
struct hash<string_ref>
{
    size_t operator() (const string_ref& s) const
    { return hash_range(s.begin(), s.end()); }
};

}

namespace std
{

bool operator < (const StringRef& lhs, const StringRef& rhs)
{
#if defined(STATIC_STRINGREF)
    return &lhs.get() < &rhs.get();
#else
    return lhs.get() < rhs.get();
#endif
}

template <>
struct hash<StringRef>
{
    size_t operator() (const StringRef& s) const
    { return boost::hash<StringRef>{}(s); }
};

bool operator == (const StringRef& lhs, const StringRef& rhs)
{
#if defined(STATIC_STRINGREF)
    return &lhs.get() == &rhs.get();
#else
    return lhs.get() == rhs.get();
#endif
}

bool operator != (const StringRef& lhs, const StringRef& rhs)
{
    return !(lhs == rhs);
}

template <typename T>
struct hash<std::vector<T>>
{
    size_t operator() (const std::vector<T>& v) const
    { return boost::hash_range(v.begin(), v.end()); }
};

template <typename T>
struct hash<std::set<T>>
{
    size_t operator() (const std::set<T>& t) const
    { return boost::hash_range(t.begin(), t.end()); }
};

}

#if defined(STRING)
using Key       = std::string;
using KeyRef    = const std::string&;
#elif defined(STRINGREF)
using Key       = StringRef;
using KeyRef    = const StringRef;
#elif defined(STRINGPTR)
using Key       = const std::string*;
using KeyRef    = const std::string*;
#elif defined(BOOSTSTRINGREF)
using Key       = boost::string_ref;
using KeyRef    = const boost::string_ref;
#elif defined(INT)
using Key       = uint32_t;
using KeyRef    = uint32_t;
#else
#error "Key needs to be defined"
#endif

template <typename T>
#if defined(SET)
using Keys_Base = std::set<T>;
#elif defined(VECTOR)
using Keys_Base = std::vector<T>;
#else
#error "Keys_Base needs to be defined"
#endif

using Keys      = Keys_Base<Key>;
using Callback  = std::function<void ()>;
#if defined(MULTIMAP)
using Map       = std::multimap<Keys, Callback>;
#elif defined(UNORDERED_MULTIMAP)
using Map       = std::unordered_multimap<Keys, Callback>;
#elif defined(MAP)
using Map       = std::map<Keys, std::vector<Callback>>;
#elif defined(UNORDERED_MAP)
using Map       = std::unordered_map<Keys, std::vector<Callback>>;
#else
#error "Map needs to be defined"
#endif

int generate_keys_size()
{
    return keys_size_dist(rng);
}

template <typename U>
void add(std::set<Key>* s, const U& u) { s->insert(u); }

template <typename U>
void add(std::vector<Key>* v, const U& u) { v->push_back(u); }

void sort(std::set<Key>*) {}
void sort(std::vector<Key>* v) { std::sort(v->begin(), v->end()); }

KeyRef i2k(const uint32_t i) {
#if defined INT
    return i;
#elif defined STRINGPTR
    return &i2s(i);
#else
    return i2s(i);
#endif
}

KeyRef generate_key(uint32_t i)
{
#if defined(RANDOM_KEYS)
    (void)i;
    return i2k(key_size_dist(rng));
#elif defined(KEYS_IN_ORDER)
    return i2k(i);
#else
#error "Key generation method needs to be defined"
#endif
}

Keys generate_keys()
{
    Keys keys;
    const auto keys_size = generate_keys_size();
#if defined(VECTOR)
    keys.reserve(keys_size);
#endif
    for (int i = 0; i != keys_size; ++i)
    {
        add(&keys, generate_key(i));
    }
    sort(&keys);
    assert(!keys.empty());
    assert(keys.size() <= MAX_KEY_SIZE);
    return keys;
}

template <typename T>
bool overlap(const T& lhs, const T& rhs)
{
    const T& small = lhs.size() < rhs.size() ? lhs : rhs;
    const T& big   = lhs.size() < rhs.size() ? rhs : lhs;

    for (const auto& e : small)
    {
        if (std::binary_search(big.begin(), big.end(), e))
        {
            return true;
        }
    }
    return false;
}

void call(const Callback& f) { f(); }
void call(const std::vector<Callback>& fs)
{
    for (const auto& f : fs)
    {
        f();
    }
}

template <typename K>
void reg(
    std::map<K, std::vector<Callback>>* m,
    const Keys& keys, 
    const Callback& f)
{
    (*m)[keys].emplace_back(f);
}

template <typename K>
void reg(
    std::unordered_map<K, std::vector<Callback>>* m,
    const Keys& keys, 
    const Callback& f)
{
    (*m)[keys].emplace_back(f);
}

template <typename M>
void reg(M* m, const Keys& keys, const Callback& f)
{
    m->emplace(keys, f);
}

int main()
{
    Map m;
    for (int i = 0; i != ENTRIES; ++i)
    {
        reg(&m, generate_keys(), []{});
    }

    for (int i = 0; i != MAX_INVOCATIONS; ++i)
    {
        const auto& keys = generate_keys();
        for (const auto& p : m)
        {
            if (overlap(p.first, keys))
            {
                call(p.second);
            }
        }
    }
}
